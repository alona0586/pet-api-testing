import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
it(`Usage is here`, async () => {
    let userData: object = {
        id: 0,
        avatar: "string",
        email: "peter242@gmail.com",
        userName: "Olenka",
        password: "12345678",

    };


    let response = await users.postUser(userData);
        expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);


        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
    });
});