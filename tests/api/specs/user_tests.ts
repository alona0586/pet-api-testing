import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
    it('test1', async () => {
        let response = await users.getAllUsers();

        console.log(response.body);
    })
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });
});