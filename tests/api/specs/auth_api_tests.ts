import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new UsersController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("peter242@gmail.com", "12345678");

        accessToken = response.body.token.accessToken.token;
        console.log(accessToken);
    });

    it(`should return 200 status code and show access token`, async () => {
        let response = accessToken;
});
});